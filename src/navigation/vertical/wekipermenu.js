export default [
 
  {
    title: 'Crear Usuario',
    route: null,
    icon: 'UserPlusIcon',
    disabled: true,
  },
  {
    title: 'Clientes',
    icon: 'UsersIcon',
    disabled: false,
    children: [
      {
        title: 'Crear Cliente',
        route: 'agregar-cliente',
      },
      {
        title: 'Listar Clientes',
        route: { name: 'listar-cliente'},
      },
      
    ],
  },
  {
    title: 'Mantenedor',
    route: null,
    icon: 'ArchiveIcon',
    disabled: true,
  },
  {
    title: 'Editar Contratos',
    route: null,
    icon: 'ClipboardIcon',
    disabled: true,
  },
  
]
